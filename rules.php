<?php

// Available placeholders: mindabartosz, LaravelBase, mindabartosz, laravelbase
return [
    'src/MyPackage.php' => 'src/LaravelBase.php',
    'config/mypackage.php' => 'config/laravelbase.php',
    'src/Facades/MyPackage.php' => 'src/Facades/LaravelBase.php',
    'src/MyPackageServiceProvider.php' => 'src/LaravelBaseServiceProvider.php',
];