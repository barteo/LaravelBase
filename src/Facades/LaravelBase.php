<?php

namespace mindabartosz\LaravelBase\Facades;

use Illuminate\Support\Facades\Facade;

class LaravelBase extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'laravelbase';
    }
}
